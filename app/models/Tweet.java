package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import play.db.jpa.GenericModel;
import play.db.jpa.Model;

@Entity
public class Tweet extends GenericModel {
	@Id
	public String id;
	public String message;
	public String date;

	@ManyToOne
	public Tweeter tweeter;

	public Tweet(String id, String message, String date, Tweeter tweeter) {
		this.id = id;
		this.message = message;
		this.date = date;
		this.tweeter = tweeter;
	}
}