package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;



public class TweetList extends Controller {

	public static void index()
	{
		List<Tweet> tweets = Tweet.findAll();
		render(tweets);
		index();
	}
  
  public static void getAllTweets()
  {
	Tweeter tweeter = Accounts.getCurrentUser();
    List<Tweet> tweets = tweeter.tweets;
    render(tweets);
  }

}