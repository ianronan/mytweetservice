package controllers;

import java.util.UUID;

import models.Tweeter;
import play.Logger;
import play.mvc.Controller;


public class Accounts extends Controller {
	public static void index() {
		render();
	}

	public static void signup() {
		render();
	}

	public static void register(String firstName, String lastName, String email, String password)
	{
	    Logger.info(firstName + " " + lastName + " " + email + " " + password);
		String uuid = UUID.randomUUID().toString();
		Tweeter tweeter = new Tweeter(uuid, firstName, lastName, email, password);
		tweeter.save();
		Home.index();
	}

	public static void login() {
		Home.index();
	}

	public static void logout() {
		session.clear();
		Accounts.index();
	}

	public static void authenticate(String email, String password) {

		Logger.info("Attempting to authenticate with " + email + ":" + password);
		Tweeter tweeter = Tweeter.findByEmail(email);
		if ((tweeter != null) && (tweeter.checkPassword(password) == true)) {
			Logger.info("Successfull authentication of " + tweeter.firstName + " " + tweeter.lastName);
			session.put("logged_in_userid", tweeter.id);
			Home.index();
		} else {
			Logger.info("Authentication failed");
			login();
		}
	}

	public static Tweeter getCurrentUser() {
		Tweeter tweeter = null;
		if (session.contains("logged_in_userid"))
			;
		{
			String userid = session.get("logged_in_userid");
			tweeter = tweeter.findById(userid);
		}
		return tweeter;
	}
}