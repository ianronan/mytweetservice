package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;



public class TweeterList extends Controller {

	public static void index()
	{
		List<Tweeter> tweeters = Tweeter.findAll();
		render(tweeters);
		index();
	}

  public static void getAllTweeters()
  {
	Tweeter tweeter = Accounts.getCurrentUser();
    List<Tweeter> Tweeters = tweeter.findAll();
    render(Tweeters);
  }
}