package controllers;

import java.text.DateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

import models.Tweet;
import models.Tweeter;
import play.Logger;
import play.mvc.Controller;

public class CreateTweet extends Controller {
	public static void index() {
		String tweeterid = session.get("logged_in_userid");

		if (tweeterid != null) {
			Tweeter tweeter = Tweeter.findById(tweeterid);
			render(tweeter);
		}
		Home.index();
	}

	public static void newTweet(String tweetmessage) {
		Tweeter tweeter = Accounts.getCurrentUser();
		String datestamp = setDate();
		String uuid = UUID.randomUUID().toString();
		Tweet tweet = new Tweet(uuid, tweetmessage, datestamp, tweeter);
		Logger.info("Tweet " + tweetmessage + " " + "Date " + datestamp + " ");
		tweeter.tweets.add(tweet);
		tweeter.save();
		Home.index();
	}

	private static String setDate() {
		DateFormat df = DateFormat.getDateTimeInstance();
		df.setTimeZone(TimeZone.getTimeZone("UTC"));
		String formattedDate = df.format(new Date());
		return formattedDate;
	}

}